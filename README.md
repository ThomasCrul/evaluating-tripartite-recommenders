# Evaluating tripartite recommenders

This repository is part of the 2022 [TU Delft Research Project](https://github.com/TU-Delft-CSE/Research-Project).

## Reproducing results

This repository contains all of the data and code used to perform the experiments shown in the paper detailing the research. To reproduce results, the Jupyter Notebook `Movielens 1M Preprocessing.ipynb` should first be used to load the MovieLens dataset and preprocess its data. Next, `Tripartite creation and evaluation.ipynb` contains the relevent code for creating the various graph options and running the experiments. Finally, `Data visualisation.ipynb` contains the data obtained from the experiments and the code used to create te plots which are shown in the paper.
